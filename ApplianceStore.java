import java.util.Scanner;

public class ApplianceStore
{
	public static void main(String[] args)
	{
		Oven[] myOvens = new Oven[4];
		
		Scanner input = new Scanner(System.in);
		
		for(int i = 0; i<myOvens.length; i++)
		{
			/*This program assumes that the user enters the correct 
			input (i.e. ints when prompted for a number, strings when
			prompted for a string, etc)*/
			
			myOvens[i] = new Oven();
			
			System.out.print("What temperature is oven number " + (i + 1) + " at currently?");
			myOvens[i].currentTemp = input.nextInt();
			
			System.out.print("What temperature should oven number " + (i + 1) + " reach?");
			myOvens[i].targetTemp = input.nextInt();
			
			System.out.print("Is the light in oven number " + (i + 1) + " on?");
			String lightOn = input.next();
			lightOn = lightOn.toLowerCase();
			
			while(!lightOn.equals("yes") && !lightOn.equals("no"))
			{
				System.out.println("Error: You must answer with yes or no.");
				lightOn = input.next();
				lightOn = lightOn.toLowerCase();
			}
			
			if(lightOn.equals("yes"))
			{
				myOvens[i].lightIsOn = true;
			}
			else
			{
				myOvens[i].lightIsOn = false;
			}
			
			System.out.print("What food are you cooking in oven number " + (i + 1) + "?");
			myOvens[i].food = input.next();
		}
		
		System.out.println();
		//To separate the results.
		
		System.out.println("Current temperature for the 4th oven: " + myOvens[3].currentTemp);
		System.out.println("Target temperature for the 4th oven: " + myOvens[3].targetTemp);
		System.out.println("The light in the 4th oven is on: " + myOvens[3].lightIsOn);
		System.out.println("Food inside the 4th oven: " + myOvens[3].food);
		
		myOvens[0].raiseTemp();
		myOvens[0].pressLightButton();
		myOvens[0].printFoodReady();
		
	}
}