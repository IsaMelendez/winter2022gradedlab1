public class Oven
{
	public boolean lightIsOn;
	public int currentTemp;
	public int targetTemp;
	public String food;
	
	public void raiseTemp()
	{
		/*For the sake of simplicity, this program will assume that the 
		oven's temperature can increase immediately.*/
		
		System.out.println("Temperature has gone from " + this.currentTemp + " to " + this.targetTemp);
		this.currentTemp = targetTemp;
	}
	
	public void pressLightButton()
	{
		if(this.lightIsOn)
		{
			System.out.println("The light is currently on. You can see your " + this.food + " inside the oven.");
			this.lightIsOn = false;
		}
		else
		{
			System.out.println("The light is currently off.");
			this.lightIsOn = true;
		}
	}
	
	public void printFoodReady()
	{
		System.out.println("Your " + this.food + " is ready!");
	}
}